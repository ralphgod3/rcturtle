#pragma once

//////////////////// SERVO REGISTERS ////////////////////

#define REGISTER_SIZE 255

//enables or disables servo control for servoNr R/W
// bit 0 = servo0
// bit 1 = servo1
//etc
#define SERVO_ENABLED 0

//enables or disable interrupt when servo reaches position R/W
// bit 0 = servo0
// bit 1 = servo1
//etc
#define SERVO_INTR_ENABLED (SERVO_ENABLED + 1)

//servo position for servos where N = servo 0-7 registers are (SERVO_N_ANGLE + N)
#define SERVO_0_ANGLE (SERVO_INTR_ENABLED + 1)
#define SERVO_1_ANGLE (SERVO_0_ANGLE + 1)
#define SERVO_2_ANGLE (SERVO_1_ANGLE + 1)
#define SERVO_3_ANGLE (SERVO_2_ANGLE + 1)
#define SERVO_4_ANGLE (SERVO_3_ANGLE + 1)
#define SERVO_5_ANGLE (SERVO_4_ANGLE + 1)
#define SERVO_6_ANGLE (SERVO_5_ANGLE + 1)
#define SERVO_7_ANGLE (SERVO_6_ANGLE + 1)

//dc motor controls
//sense is the meassured rpm of the motor and is updated by the rp2040 automaticaly
//speed is a int16_t that is used to control motor direction and speed.
#define MOTOR_0_SPEED (SERVO_7_ANGLE + 1)
#define MOTOR_0_SENSE (MOTOR_0_SPEED + 1)
#define MOTOR_1_SPEED (MOTOR_0_SENSE + 1)
#define MOTOR_1_SENSE (MOTOR_1_SPEED + 1)
#define MOTOR_2_SPEED (MOTOR_1_SENSE + 1)
#define MOTOR_2_SENSE (MOTOR_2_SPEED + 1)
#define MOTOR_3_SPEED (MOTOR_2_SENSE + 1)
#define MOTOR_3_SENSE (MOTOR_3_SPEED + 1)


//adc controls
#define ADC_SAMPLERATE (MOTOR_3_SENSE + 1)
#define ADC_0_RESULT (ADC_SAMPLERATE + 1)
#define ADC_1_RESULT (ADC_0_RESULT + 1)
#define ADC_2_RESULT (ADC_1_RESULT` + 1)