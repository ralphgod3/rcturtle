#pragma once
#include <vector>

class ADCOperations
{
private:
    uint16_t p_timeBetweenSamples = 0;
    std::vector<uint8_t> p_pins = std::vector<uint8_t>();
    std::vector<uint16_t> p_values = std::vector<uint16_t>();
    unsigned long long p_lastMillis = 0;

public:
    ADCOperations(){};
    ~ADCOperations(){};

    void init(std::vector<uint8_t>& pins, uint16_t timeBetweenSamples = 1000)
    {
        analogReadResolution(16);
        for(int i = 0; i < pins.size(); i++)
        {
            pinMode(pins[i],INPUT);
            p_pins.push_back(pins[i]);
            p_values.push_back(0);
        }
        p_timeBetweenSamples = timeBetweenSamples;
    }

    void handle()
    {
        if(p_lastMillis + p_timeBetweenSamples < millis())
        {
            for(int i = 0; i < p_pins.size(); i++)
            {
                p_values[i] = analogRead(p_pins[i]);
            }
            p_lastMillis = millis();
        }
    }

    uint16_t getValue(uint8_t adcNum)
    {
        return p_values[adcNum];
    }
};