#pragma once

//user changeable configuration for the rp2040




//////////////////// COMMUNICATION SETTINGS ////////////////////

//debug baudrate
const constexpr uint32_t BAUDRATE = 115200;

//address we will listen to on i2c bus
const constexpr uint8_t I2CADDR = 2;



//////////////////// PIN DEFINITIONS ////////////////////

//pins that the servos are connected to in order of 0-7
const constexpr uint8_t SERVOPINS[] = {0,1,2,3,19,20,21,22};

//pins connected with esp32
//i2c should operate in SLAVE mode only to prevent issues
const constexpr uint8_t I2CSDA = 4;
const constexpr uint8_t I2CSCL = 5;
//pin we set high when a interrupt event happens so esp can read interrupt source.
const constexpr uint8_t I2CINTR = 6;

//motor pins
//direction is a digital output
//speed is a pwm output
//HAL is a interrupt enabled input

const constexpr uint8_t MOTOR1DIRECTION =7;
const constexpr uint8_t MOTOR1SPEED = 8;
const constexpr uint8_t MOTOR1HAL = 15;

const constexpr uint8_t MOTOR2DIRECTION = 9;
const constexpr uint8_t MOTOR2SPEED = 10;
const constexpr uint8_t MOTOR2HAL = 16;

const constexpr uint8_t MOTOR3DIRECTION = 11;
const constexpr uint8_t MOTOR3SPEED = 12;
const constexpr uint8_t MOTOR3HAL = 17;

const constexpr uint8_t MOTOR4DIRECTION = 13;
const constexpr uint8_t MOTOR4SPEED = 14;
const constexpr uint8_t MOTOR4HAL = 18;

//ADC1 & 2 are analog inputs
const constexpr uint8_t ADC1 = 26;
const constexpr uint8_t ADC2 = 27;
const constexpr uint8_t ADC3 = 28;
