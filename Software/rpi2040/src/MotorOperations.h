#pragma once
#include <Arduino.h>
#include <vector>

struct motorInfo
{
    uint8_t dirPin;
    uint8_t speedPin;
    uint8_t halPin;
    motorInfo(uint8_t dirPin, uint8_t speedPin, uint8_t halPin)
    {
        this->dirPin = dirPin;
        this->speedPin = speedPin;
        this->halPin = halPin;
    }
};

class MotorOperations
{
private:
    std::vector<motorInfo> p_motorInfo = std::vector<motorInfo>();

public:
    MotorOperations(){};
    ~MotorOperations(){};

    void init(std::vector<motorInfo> &motorInfo)
    {
        analogWriteResolution(15);
        for (int i = 0; i < motorInfo.size(); i++)
        {
            pinMode(motorInfo[i].dirPin, OUTPUT);
            pinMode(motorInfo[i].speedPin, OUTPUT);
            pinMode(motorInfo[i].halPin, INPUT_PULLDOWN);
            p_motorInfo.push_back(motorInfo[i]);
        }
    }

    void setSpeed(uint8_t motor, int16_t speed)
    {
        if (speed < 0)
        {
            digitalWrite(p_motorInfo[motor].dirPin, false);
        }
        else
        {
            digitalWrite(p_motorInfo[motor].dirPin, true);
        }
        if (speed != 0)
        {
            analogWrite(p_motorInfo[motor].speedPin, abs(speed));
        }
        else
        {
            analogWrite(p_motorInfo[motor].speedPin, 0);
        }
    }

    // todo: implement rest of dc motor registers
};