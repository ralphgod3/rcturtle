#pragma once

#include <vector>
#include "Servo.h"

class ServoOperations
{
private:
    std::vector<Servo> p_servos;
    std::vector<uint8_t> p_servoPins;
    std::vector<uint8_t> p_servoAngles;

public:
    ServoOperations(){};
    ~ServoOperations(){};
    
    /**
     * @brief initialize servo class
     * 
     * @param servoPins pins array that have servos attached
     * @param servoPinSize size of servoPins array
     */
    void init(uint8_t *servoPins, uint8_t servoPinSize)
    {
        for(uint8_t i = 0; i < servoPinSize; i++)
        {
            p_servoAngles.push_back(0);
            p_servoPins.push_back(servoPins[i]);
            p_servos.push_back(Servo());
            p_servos[i].write(90);
        }
    }

    /**
     * @brief enables or disables servo based on "register" values given
     * 
     * @param oldVal value register had before change
     * @param newVal value register should have after change
     */
    void handleServoEnabled(uint8_t oldVal, uint8_t newVal)
    {
        if(oldVal == newVal)
        {
            return;
        }
        for(int i = 0; i < 8; i++)
        {
            uint8_t newValSeperated = (newVal & (1 << i));
            if((oldVal & (1 << i))  != newValSeperated)
            {
                if(newValSeperated > 0)
                {
                    Serial.print("attaching servo ");
                    Serial.println(p_servoPins[i]);
                    p_servos[i].attach(p_servoPins[i]);
                    p_servos[i].write(p_servoAngles[i]);
                }
                else
                {
                    Serial.print("detaching servo ");
                    Serial.println(p_servoPins[i]);
                    p_servos[i].detach();
                }
            }
        }
    }

    /**
     * @brief Set the Servo Angle
     * 
     * @param servo servo to set angle for
     * @param angle angle to set servo at
     */
    void setServoAngle(uint8_t servo, uint8_t angle)
    {
        p_servoAngles[servo] = angle;
        p_servos[servo].write(angle);
    }
};