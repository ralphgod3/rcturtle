// arduino includes
#include <Wire.h>
// project includes

#include "src/configuration.h"
#include "src/registermap.h"
#include "src/ServoOperations.h"
#include "src/MotorOperations.h"
#include "src/ADCOperations.h"

#include <vector>
#include <list>

void onReceive(int howMany);
void onRequest();

// get array size for a constant size array
#define ARRAYSIZE(arr) (sizeof(arr) / sizeof(arr[0]))

ServoOperations servos;
MotorOperations motors;
ADCOperations adc;

void setup()
{
    // initialize all pins and hardware
    Serial.begin(BAUDRATE, SERIAL_8N1);

    servos.init(const_cast<uint8_t *>(SERVOPINS), ARRAYSIZE(SERVOPINS));

    std::vector<motorInfo> motInfo = std::vector<motorInfo>();

    motInfo.push_back(motorInfo(MOTOR1DIRECTION, MOTOR1SPEED, MOTOR1HAL));
    motInfo.push_back(motorInfo(MOTOR2DIRECTION, MOTOR2SPEED, MOTOR2HAL));
    motInfo.push_back(motorInfo(MOTOR3DIRECTION, MOTOR3SPEED, MOTOR3HAL));
    motInfo.push_back(motorInfo(MOTOR4DIRECTION, MOTOR4SPEED, MOTOR4HAL));
    motors.init(motInfo);

    std::vector<uint8_t> adcPins = std::vector<uint8_t>();
    adcPins.push_back(ADC1);
    adcPins.push_back(ADC2);
    adcPins.push_back(ADC3);
    adc.init(adcPins);

    pinMode(I2CINTR, OUTPUT);
    // todo: check if we need to define pins here
    Wire.begin(I2CADDR);
    Wire.onReceive(onReceive);
    Wire.onRequest(onRequest);
}

struct command
{
    uint16_t reg = 0;
    uint8_t oldVal = 0;
    uint8_t newVal = 0;
    command(uint16_t reg, uint8_t oldVal, uint8_t newVal)
    {
        reg = reg;
        oldVal = oldVal;
        newVal = newVal;
    }
};

// registerDefinition
uint8_t registers[REGISTER_SIZE] = {0};

// value for last accessed register
uint8_t registerVal = 0;
// command queue for register writes
std::list<command> commandQueue = std::list<command>();

void onReceive(int howMany)
{
    // read register from i2c
    uint16_t reg = Wire.read();
    uint8_t newVal = 0;
    if (reg > REGISTER_SIZE)
    {
        registerVal = 255;
        return;
    }

    registerVal = registers[reg];

    // if more data is sent that means we have to write it
    if (howMany > 1)
    {
        newVal = Wire.read();
        commandQueue.push_back(command(reg, registers[reg], newVal));
        registers[reg] = newVal;
    }

    // clear out any leftover data from buffer
    while (Wire.available() > 0)
    {
        Wire.read();
    }
}

void onRequest()
{
    Wire.write(registerVal);
}

void loop()
{
    //process commands
    while(commandQueue.size() > 0)
    {
        command& com = *commandQueue.begin();
        switch (com.reg)
        {
        case SERVO_ENABLED:
            servos.handleServoEnabled(com.oldVal, com.newVal);
            break;
        case SERVO_INTR_ENABLED:
            //empty for now
            break;
        case SERVO_0_ANGLE:
            servos.setServoAngle(0, com.newVal);
            break;
        case SERVO_1_ANGLE:
            servos.setServoAngle(1, com.newVal);
            break;
        case SERVO_2_ANGLE:
            servos.setServoAngle(2, com.newVal);
            break;
        case SERVO_3_ANGLE:
            servos.setServoAngle(3, com.newVal);
            break;
        case SERVO_4_ANGLE:
            servos.setServoAngle(4, com.newVal);
            break;
        case SERVO_5_ANGLE:
            servos.setServoAngle(5, com.newVal);
            break;
        case SERVO_6_ANGLE:
            servos.setServoAngle(6, com.newVal);
            break;
        case SERVO_7_ANGLE:
            servos.setServoAngle(7, com.newVal);
            break;
        default:
            Serial.print("invalid register ");
            Serial.println(com.reg);
            break;
        }


        commandQueue.pop_front();
    }

    //update adc
    adc.handle();
}