# RP2040

## info

This code should be flashed onto the raspberry pi pico and provides an i2c interface to the esp32 to control the servos, dc motors and analog inputs connected to the raspberry pi pico.  
In most of the documentation I will refer to the raspberry pi pico as the RP2040 since its shorter and i may make a PCB with just the bare chip.  
  
Software on the RP2040 does NOT have to be reflashed if you are updating the esp32 unless you are adding or changing functionality in the RP2040.  
ie: changing how registers or pin control works would require a change in the RP2040 code whilst changing servo angles would only require esp32 code changes.  

## Registers

The esp32 can talk to the rp2040 using registers, This is a standard method of interfacing with peripherals in embedded and non embedded systems but is abstracted by pretty much any programming language.  

Below here a register map will be published indicating what register sets what options with examples as to how to do basic operations.

REGISTER MAP WILL BE ADDED LATER

## Disclaimer

I do not like Arduino and dont care about the slave code enough to make it pretty or optimize it further than it is.  
It is currently optimized for speed when responding to i2c reqeusts not memory usage since we will not hit memory limits on the RP2040 anytime soon when it just functions as a slave.
